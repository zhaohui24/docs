# 知识框架导航站



------
- [GNN-图神经网络-笔记](https://blog.csdn.net/weixin_31948131/article/details/120275241)
    - 傅里叶变换，拉普拉斯矩阵

-  **lead-lag** 
    - [Estimation of the lead-lag from non-synchronous data](https://github.com/philipperemy/lead-lag)
        - 未精确到微秒级别 
    - 

- [leetcode 刷题](https://gitee.com/zhaohui24/algorithm-problem/tree/master)


# C++ 语法
### 一、逻辑运算与位运算符

| 逻辑运算符 |      | 位运算符 |
| :--------: | :--: | :------: |
|     &&     |  与  |    &     |
|    \|\|    |  或  |    \|    |
|     ！     |  非  |    ~     |
|            | 异或 |    ^     |

- 一元运算符优先级大于二元运算符； 位运算符大于逻辑运算符；  ~！> & > |




### 二、指针

- 指针两要素： **内存地址** 和 **内存大小**（数据类型int, char ...）  
- 取址符`&`;      `int a{10};   int *pa =&a;`




​	**指针数组**

​		`int* pa[5];`    代表 五个int 类型的指针

​	**数组指针**    本质上是指针

​		`int (*pb)[5]{};`      每一行放5个数据

```c++
int testA[2][5]
{
	{1,2,3,4,5},
	{4,5,6,7,8}
};

int (*pb)[5]{testA};   // 指针数组
pb = pb + 1;  //  +1 *数据类型的大小， 5*int型大小（二维）
			 // 若是1维数组，地址 直接加上数据类型大小
```





- 从原理上讲，指针和数组是同一个方法的不同表达，而数组名本身就是一个指针，数组元素只是这个指针按照一定量偏移后对应的内存区域的内容。
- 数组的本质是连续的内存区域，多维数组是创建出来的直观理解。


### 三、动态内存分配

##### C 语言中内存分配

​	`malloc`   

​	`void * malloc(size_t size);	`

​	将为用户分配 size_t 字节个内存，并且返回内存分配的地址，若分配失败，则返回 0	 

```c++
int* pa = (int *)malloc(4);
```

 pa 是分配好的内存地址， 4 是要分配的大小 。若内存分配失败，那么 pa=0

- 空指针判断 `if (pa==nullptr)`


---




​	`calloc`

​	`void* calloc(size_t count, size_t size);	`

​	calloc 将为用户分配 count 乘 size_t 字节个内存，并且返回内存分配的地址，若分配失败，则返回 0	

```c++
int* p = (int *)calloc(1, 4);
```

​	p 是分配好的内存的地址， 1 是要分配的元素个数，4 是要分配的每个元素的大小 。

​	若内存分配失败，那么 pa=0， calloc 会将分配好的内存区域设置为 0

---



​	`realloc`

​	`void* realloc(void* _Block, size_t _Size);`

​	realloc 将为用户重新分配内存，_Block 是用户已经分配好的内存，_Size是要求重新分配的大小，函数返回重新分配后的内存。

```c++
int*  pa = (int *)malloc(4);
pa = (int *) realloc(pa, 8); 
```
​	pa 是重新分配后的内存的地址 8 重新分配后的大小。若内存分配失败，那么 pa=0

---



​	`free`     释放内存

​	`void* free(void* _Block);`

​	Block 是需要释放的内存地址

```c++
int* pa = (int *)malloc(4);
free(pa)
```

​	pa 所占用的内存被释放

---



##### C++ 内存分配

数据类型*  指针变量名称 = new 数据类型；

`int* pa = new int{};`

 数据类型*  指针变量名称 = new 数据类型【数量】；

`int* pa= new int[5]{};  `      

​	分配一段能存放5个int 变量类型的内存空间



`delete `   释放指针

​	`delete pa;`    ,  `delete[] pa;`



---



##### 智能指针

​	`std::unique_ptr`  解决原生指针安全性不足的弊端

```c++
std::unique_ptr<int> intPtr{std::make_unique<int>(50)};   // 唯一智能指针，初始化为50
std::cout<<*intPtr;

std::unique_ptr<int []> ptrA{std::make_unique<int[]>(10)};
ptrA[0] = 50;

intPtr.reset();   // 释放指针空间

a = intPtr.get();  // 得到intPtr 指向申请内存时的地址

intPtr.release();  // 将智能指针intPtr设置为nullptr，但没有释放指针占用的空间

// std::unique_ptr 具有唯一性,不可复制，但可以转移
std::unique_ptr<int> ptrAA{std::make_unique<int>(10)};
std::unique_ptr<int> ptrBB{};
ptrBB=std::move(ptrAA);  // 转移后 ptrAA 被设置为 nullptr
```

​	

​	